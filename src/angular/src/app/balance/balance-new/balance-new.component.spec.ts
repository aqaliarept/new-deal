import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceNewComponent } from './balance-new.component';

describe('BalanceNewComponent', () => {
  let component: BalanceNewComponent;
  let fixture: ComponentFixture<BalanceNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
