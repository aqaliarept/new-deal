import { Operations } from "./operations";
import { Economy } from "../domain/economy";
import { EconomyValuesTester } from "../domain/economy.spec";
import { EconomyElements } from "../domain/economyElements";

describe('Operations', () => {

    let economy: Economy;
  
    beforeEach((() => {
      economy = new Economy({
        treasuryTDeposits : 40,
        treasuryTTL : 0,
        centralBankTreasuries : 170,
        centralBankEquity : 10,
        banksCurrency : 40,
        banksLoans : 0,
        banksReserves : 80,
        banksTreasuries : 0,
        householdCurrency : 0,
        householdDeposits : 40,
        householdTreasuries : 40,
        companiesDeposits : 40,
        companiesBonds : 0,
        companiesSecurLoans : 0
      })
    }));
  
    it('Private Spending', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.householdEquity, value : -value},
          {key : EconomyElements.companiesDeposits, value : value},
          {key : EconomyElements.companiesEquity, value : value},
        ];

        TestChangedValues(Operations.privateSpending, getChanges, economy, values, 7);
        TestChangedValues(Operations.privateSpending, getChanges, economy, values, -13);
    });
  
    it('Government Spends', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryNegEquity, value : value},
          {key : EconomyElements.treasuryTDeposits, value : -value},
          {key : EconomyElements.banksReserves, value : value},
          {key : EconomyElements.banksDeposits, value : value},
          {key : EconomyElements.householdDeposits, value : value},
          {key : EconomyElements.householdEquity, value : value},
          {key : EconomyElements.fedGovNegEquity, value : value},
          {key : EconomyElements.fedGovAssets, value : -value},
          {key : EconomyElements.privateAssets, value : value * 2},
          {key : EconomyElements.privateEquity, value : value},
          {key : EconomyElements.privateLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value},
          {key : EconomyElements.totalLiabilities, value : value},
        ];

        TestChangedValues(Operations.governmentSpending, getChanges, economy, values, 7);
        TestChangedValues(Operations.governmentSpending, getChanges, economy, values, -13);
    });
  
    it('Government Taxes', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryNegEquity, value : -value},
          {key : EconomyElements.treasuryTDeposits, value : value},
          {key : EconomyElements.banksReserves, value : -value},
          {key : EconomyElements.banksDeposits, value : -value},
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.householdEquity, value : -value},
          {key : EconomyElements.fedGovNegEquity, value : -value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.privateAssets, value : -value * 2},
          {key : EconomyElements.privateEquity, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
        ];

        TestChangedValues(Operations.governmentTaxes, getChanges, economy, values, 7);
        TestChangedValues(Operations.governmentTaxes, getChanges, economy, values, -13);
    });
  
    it('Government Issues Debt', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryTreasuries, value : value},
          {key : EconomyElements.treasuryTDeposits, value : value},
          {key : EconomyElements.banksReserves, value : -value},
          {key : EconomyElements.banksDeposits, value : -value},
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.householdTreasuries, value : value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
        ];

        TestChangedValues(Operations.governmentDebtIssuance, getChanges, economy, values, 7);
        TestChangedValues(Operations.governmentDebtIssuance, getChanges, economy, values, -13);
    });
  
    it('Government Spends (Consolidated)', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryTreasuries, value : value},
          {key : EconomyElements.treasuryNegEquity, value : value},
          {key : EconomyElements.householdEquity, value : value},
          {key : EconomyElements.householdTreasuries, value : value},
          {key : EconomyElements.fedGovNegEquity, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value},
          {key : EconomyElements.privateAssets, value : value},
          {key : EconomyElements.privateEquity, value : value},
          {key : EconomyElements.totalLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value},
        ];

        TestChangedValues(Operations.consolidatedGovernmentSpending, getChanges, economy, values, 7);
        TestChangedValues(Operations.consolidatedGovernmentSpending, getChanges, economy, values, -13);
    });
  
    it('Government Spends (Without Borrowing)', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryTreasuries, value : value},
          {key : EconomyElements.treasuryNegEquity, value : value},
          {key : EconomyElements.centralBankDeposits, value : value},
          {key : EconomyElements.centralBankTreasuries, value : value},
          {key : EconomyElements.banksDeposits, value : value},
          {key : EconomyElements.banksReserves, value : value},
          {key : EconomyElements.householdEquity, value : value},
          {key : EconomyElements.householdDeposits, value : value},
          {key : EconomyElements.fedGovNegEquity, value : value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value * 2},
          {key : EconomyElements.privateAssets, value : value * 2},
          {key : EconomyElements.privateEquity, value : value},
          {key : EconomyElements.privateLiabilities, value : value},
          {key : EconomyElements.totalLiabilities, value : value * 3},
          {key : EconomyElements.totalAssets, value : value * 3},
        ];

        TestChangedValues(Operations.governmentSpendingNoBonds, getChanges, economy, values, 7);
        TestChangedValues(Operations.governmentSpendingNoBonds, getChanges, economy, values, -13);
    });
  
    it('Bank Customer Withdraws Currency', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.banksCurrency, value : -value},
          {key : EconomyElements.banksDeposits, value : -value},
          {key : EconomyElements.householdCurrency, value : value},
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
        ];

        TestChangedValues(Operations.withdrawCurrency, getChanges, economy, values, 13);
        TestChangedValues(Operations.withdrawCurrency, getChanges, economy, values, -7);
    });
  
    it('Bank Customer Deposits Currency', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.banksCurrency, value : value},
          {key : EconomyElements.banksDeposits, value : value},
          {key : EconomyElements.householdCurrency, value : -value},
          {key : EconomyElements.householdDeposits, value : value},
          {key : EconomyElements.privateAssets, value : value},
          {key : EconomyElements.privateLiabilities, value : value},
          {key : EconomyElements.totalLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value},
        ];

        TestChangedValues(Operations.depositCurrency, getChanges, economy, values, -13);
        TestChangedValues(Operations.depositCurrency, getChanges, economy, values, 7);
    });

    let bankLoanChanges = (value: number) => [
      {key : EconomyElements.banksLoans, value : value},
      {key : EconomyElements.banksDeposits, value : value},
      {key : EconomyElements.householdLoans, value : value},
      {key : EconomyElements.householdDeposits, value : value},
      {key : EconomyElements.privateAssets, value : value * 2},
      {key : EconomyElements.privateLiabilities, value : value * 2},
      {key : EconomyElements.totalLiabilities, value : value * 2},
      {key : EconomyElements.totalAssets, value : value * 2},
    ];
  
    it('Bank Loan', () => {
        let values = EconomyValuesTester.GetDefaultValues();

        TestChangedValues(Operations.bankLoan, bankLoanChanges, economy, values, 13);
        TestChangedValues(Operations.bankLoan, bankLoanChanges, economy, values, -7);
    });
  
    it('Borrower Pays Interest on Bank Loan', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.banksEquity, value : value},
          {key : EconomyElements.banksDeposits, value : -value},
          {key : EconomyElements.householdEquity, value : -value},
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
        ];

        TestChangedValues(Operations.bankLoanInterest, getChanges, economy, values, 13);
        TestChangedValues(Operations.bankLoanInterest, getChanges, economy, values, -7);
    });
  
    it('Borrower Repays Principal from Bank Loan', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.banksLoans, value : -value},
          {key : EconomyElements.banksDeposits, value : -value},
          {key : EconomyElements.householdLoans, value : -value},
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.privateAssets, value : -value * 2},
          {key : EconomyElements.privateLiabilities, value : -value * 2},
          {key : EconomyElements.totalLiabilities, value : -value * 2},
          {key : EconomyElements.totalAssets, value : -value * 2},
        ];

        TestChangedValues(Operations.bankLoanRepayment, getChanges, economy, values, -13);
        TestChangedValues(Operations.bankLoanRepayment, getChanges, economy, values, 7);
    });
  
    it('Borrower Defaults on Bank Loan', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.banksLoans, value : -value},
          {key : EconomyElements.banksEquity, value : -value},
          {key : EconomyElements.householdLoans, value : -value},
          {key : EconomyElements.householdEquity, value : value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
        ];

        TestChangedValues(Operations.bankLoanDefault, getChanges, economy, values, -13);
        TestChangedValues(Operations.bankLoanDefault, getChanges, economy, values, 7);
    });
  
    it('Private Bond Issued', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.householdBonds, value : value},
          {key : EconomyElements.householdDeposits, value : -value},
          {key : EconomyElements.companiesDeposits, value : value},
          {key : EconomyElements.companiesBonds, value : value},
          {key : EconomyElements.privateAssets, value : value},
          {key : EconomyElements.privateLiabilities, value : value},
          {key : EconomyElements.totalLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value},
        ];

        TestChangedValues(Operations.bondIssuance, getChanges, economy, values, 13);
        TestChangedValues(Operations.bondIssuance, getChanges, economy, values, -7);
    });
  
    it('Private Bond Interest Paid', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.householdEquity, value : value},
          {key : EconomyElements.householdDeposits, value : value},
          {key : EconomyElements.companiesDeposits, value : -value},
          {key : EconomyElements.companiesEquity, value : -value},
        ];

        TestChangedValues(Operations.bondInterest, getChanges, economy, values, 13);
        TestChangedValues(Operations.bondInterest, getChanges, economy, values, -7);
    });
  
    it('Private Bond Principal Repaid', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.householdBonds, value : -value},
          {key : EconomyElements.householdDeposits, value : value},
          {key : EconomyElements.companiesDeposits, value : -value},
          {key : EconomyElements.companiesBonds, value : -value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
        ];

        TestChangedValues(Operations.bondRepayment, getChanges, economy, values, -13);
        TestChangedValues(Operations.bondRepayment, getChanges, economy, values, 7);
    });
  
    it('Private Bond Defaulted On', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.householdBonds, value : -value},
          {key : EconomyElements.householdEquity, value : -value},
          {key : EconomyElements.companiesEquity, value : value},
          {key : EconomyElements.companiesBonds, value : -value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
        ];

        TestChangedValues(Operations.bondDefault, getChanges, economy, values, -13);
        TestChangedValues(Operations.bondDefault, getChanges, economy, values, 7);
    });
  
    it('Bank Loan is Securitized', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.banksLoans, value : -value},
          {key : EconomyElements.banksDeposits, value : -value},
          {key : EconomyElements.companiesDeposits, value : -value},
          {key : EconomyElements.companiesSecurLoans, value : value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
        ];

        TestChangedValues(Operations.bankLoan, bankLoanChanges, economy, values, 27);
        TestChangedValues(Operations.bankLoanSecuritize, getChanges, economy, values, 13);
        TestChangedValues(Operations.bankLoanSecuritize, getChanges, economy, values, -7);
    });
  
    it('Quantitative Easing (Variation 1 - Households Sell)', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.centralBankTreasuries, value : value},
          {key : EconomyElements.centralBankDeposits, value : value},
          {key : EconomyElements.banksReserves, value : value},
          {key : EconomyElements.banksDeposits, value : value},
          {key : EconomyElements.householdTreasuries, value : -value},
          {key : EconomyElements.householdDeposits, value : value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value},
          {key : EconomyElements.privateAssets, value : value},
          {key : EconomyElements.privateLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value * 2},
          {key : EconomyElements.totalLiabilities, value : value * 2},
        ];

        TestChangedValues(Operations.qe, getChanges, economy, values, 13);
        TestChangedValues(Operations.qe, getChanges, economy, values, -7);
    });
  
    it('Quantitative Easing (Variation 2 - Banks Sell)', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.centralBankTreasuries, value : value},
          {key : EconomyElements.centralBankDeposits, value : value},
          {key : EconomyElements.banksReserves, value : value},
          {key : EconomyElements.banksTreasuries, value : -value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value},
          {key : EconomyElements.totalLiabilities, value : value},
        ];

        TestChangedValues(Operations.qeBanks, getChanges, economy, values, -13);
        TestChangedValues(Operations.qeBanks, getChanges, economy, values, 7);
    });
  
    it('Open Market Ops: Raise Rates Toward Target', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.centralBankTreasuries, value : -value},
          {key : EconomyElements.centralBankDeposits, value : -value},
          {key : EconomyElements.banksReserves, value : -value},
          {key : EconomyElements.banksTreasuries, value : value},
          {key : EconomyElements.fedGovAssets, value : -value},
          {key : EconomyElements.fedGovLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
        ];

        TestChangedValues(Operations.omoRaiseRates, getChanges, economy, values, 13);
        TestChangedValues(Operations.omoRaiseRates, getChanges, economy, values, -7);
    });
  
    it('Open Market Ops: Lower Rates Toward Target', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.centralBankTreasuries, value : value},
          {key : EconomyElements.centralBankDeposits, value : value},
          {key : EconomyElements.banksReserves, value : value},
          {key : EconomyElements.banksTreasuries, value : -value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value},
          {key : EconomyElements.totalLiabilities, value : value},
        ];

        TestChangedValues(Operations.omoLowerRates, getChanges, economy, values, -13);
        TestChangedValues(Operations.omoLowerRates, getChanges, economy, values, 7);
    });
  
    it('Government Issues Debt (Banks Buy via TT&L)', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryTTL, value : value},
          {key : EconomyElements.treasuryTreasuries, value : value},
          {key : EconomyElements.banksTTL, value : value},
          {key : EconomyElements.banksTreasuries, value : value},
          {key : EconomyElements.fedGovAssets, value : value},
          {key : EconomyElements.fedGovLiabilities, value : value},
          {key : EconomyElements.privateAssets, value : value},
          {key : EconomyElements.privateLiabilities, value : value},
          {key : EconomyElements.totalAssets, value : value * 2},
          {key : EconomyElements.totalLiabilities, value : value * 2},
        ];

        TestChangedValues(Operations.governmentDebtIssuanceTtl, getChanges, economy, values, 13);
        TestChangedValues(Operations.governmentDebtIssuanceTtl, getChanges, economy, values, -7);
    });
  
    it('Treasury Moves Funds from Bank TT&L to Central Bank', () => {
        let values = EconomyValuesTester.GetDefaultValues();
        let getChanges = (value: number) => [
          {key : EconomyElements.treasuryTTL, value : -value},
          {key : EconomyElements.treasuryTDeposits, value : value},
          {key : EconomyElements.banksTTL, value : -value},
          {key : EconomyElements.banksReserves, value : -value},
          {key : EconomyElements.privateAssets, value : -value},
          {key : EconomyElements.privateLiabilities, value : -value},
          {key : EconomyElements.totalAssets, value : -value},
          {key : EconomyElements.totalLiabilities, value : -value},
        ];

        TestChangedValues(Operations.treasuryMovestTtl, getChanges, economy, values, -13);
        TestChangedValues(Operations.treasuryMovestTtl, getChanges, economy, values, 7);
    });
});

function TestChangedValues(operation, 
  getChanges : (value : number) => {key : string, value : number}[], 
  economy : Economy, 
  values : Map<string, number>, 
  difference : number)
{
  economy.applyCommands(operation.func(difference));

  var changes = getChanges(difference);
  changes.forEach(change => {
    var value = values.get(change.key);
    value += change.value;
    values.set(change.key, value);
  });
  EconomyValuesTester.CheckValues(economy, values);
}