import { Component, Input, OnInit, Output,  EventEmitter } from '@angular/core';

export interface ISectorViewModel{
  name: string;
  translation: string;
  sum: number;
  assets: {
    id: string,
    name: string,
    value: number,
    color: string,
    delta: number
  }[];
  liabilities: {
    id: string,
    name: string,
    value: number,
    color: string,
    delta: number
  }[];
};


@Component({
  selector: 'app-pair-chart',
  templateUrl: './pair-chart.component.html',
  styleUrls: ['./pair-chart.component.scss']
})
export class PairChartComponent implements OnInit {
  @Input()
  sector: ISectorViewModel;
  @Input()
  scale: number;
  @Output()
  elementClicked : EventEmitter<string> = new EventEmitter<string>();
  @Input()
  selectedElementId: string;


  constructor() { }

  ngOnInit(): void {
  }

  trackElement(index, item){
    return item.id;
  }

  onClick(elementId: string) {
    this.elementClicked.emit(elementId);
  }
}
