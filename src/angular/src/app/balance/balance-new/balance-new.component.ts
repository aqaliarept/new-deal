import { Component, OnDestroy, OnInit } from '@angular/core';
import { Economy, Sector } from '../domain/economy';
import { IOperation } from "../domain/operation";
import { EconomyElements } from "../domain/economyElements";
import { ISectorViewModel } from './pair-chart/pair-chart.component';
import { timer, Subscription } from 'rxjs';
import { Operations } from './operations';
import { ViewEncapsulation } from '@angular/core'


@Component({
  selector: 'app-balance-new',
  // templateUrl: './balance-new-design.component.html',
  templateUrl: './balance-new.component.html',
  styleUrls: ['./balance-new.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BalanceNewComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  economy: Economy;

  elements = EconomyElements;
  selectedOperation: {name: string, description: string, func : (value: number) => Partial<IOperation>};
  operations = Operations.operations;

  selectedElementId: string = null;
  value: number = 10;
  error: string;

  total: ISectorViewModel;
  federalGovernment: ISectorViewModel;
  treasury: ISectorViewModel;
  centralBank: ISectorViewModel;
  banks: ISectorViewModel;
  houseHolds: ISectorViewModel;
  companies: ISectorViewModel;
  privateSector: ISectorViewModel;





  private colors = ["#ca3a3a", "#5756c5", "#007d00", "#e29537", "#1d1f38"];

  elementDescriptions: Map<string, {name:string, translation:string, description: string}> = new Map<string, {name:string, translation:string, description: string}>([
    [EconomyElements.totalAssets, {name: "Assets", translation: "Активы", description: "Отражают состав и стоимость имущества, находящегося во владении юридических и физических лиц. В общем случае в состав активов входят требования данного лица/организации/сектора (А) в отношении лиц/организаций/должников (Б), у которых есть долги и иные обязательства перед А."}],
    [EconomyElements.totalLiabilities, {name: "Liabilities", translation: "Обязательства", description: "Задолженность лица или организации перед её контрагентами (например, банковские депозиты клиентов). В качестве частного случая обязательством является также задолженность перед своими владельцами (фактически собственно имущество организации). Для общества в целом аналогом такого понятия служат «чистые активы» (net worth), то есть реальная стоимость имущества (накопленное богатство) за вычетом долгов."}],

    [EconomyElements.fedGovAssets, {name: "Assets", translation: "Активы", description: "Отражают состав и стоимость имущества, находящегося во владении юридических и физических лиц. В общем случае в состав активов входят требования данного лица/организации/сектора (А) в отношении лиц/организаций/должников (Б), у которых есть долги и иные обязательства перед А."}],
    [EconomyElements.fedGovLiabilities, {name: "Liabilities", translation: "Обязательства", description: "Задолженность лица или организации перед её контрагентами (например, банковские депозиты клиентов). В качестве частного случая обязательством является также задолженность перед своими владельцами (фактически собственно имущество организации). Для общества в целом аналогом такого понятия служат «чистые активы» (net worth), то есть реальная стоимость имущества (накопленное богатство) за вычетом долгов."}],
    [EconomyElements.fedGovNegEquity, {name: "Neg.Equity", translation: "Отриц. собственный капитал", description: "Собственный капитал правительства (отрицательный) равен чистым активам частного сектора (положительным), поскольку обязательства государства являются активами частного сектора (чей-то долг связан с чьими-то правами требования). Таким образом, долг одного сектора является богатством другого (гособлигации — долг государства и часть богатства частного сектора)"}],

    [EconomyElements.privateAssets, {name: "Assets", translation: "Активы", description: "Отражают состав и стоимость имущества, находящегося во владении юридических и физических лиц. В общем случае в состав активов входят требования данного лица/организации/сектора (А) в отношении лиц/организаций/должников (Б), у которых есть долги и иные обязательства перед А."}],
    [EconomyElements.privateLiabilities, {name: "Liabilities", translation: "Обязательства", description: "Задолженность лица или организации перед её контрагентами (например, банковские депозиты клиентов). В качестве частного случая обязательством является также задолженность перед своими владельцами (фактически собственно имущество организации). Для общества в целом аналогом такого понятия служат «чистые активы» (net worth), то есть реальная стоимость имущества (накопленное богатство) за вычетом долгов."}],
    [EconomyElements.privateEquity, {name: "Equity", translation: "Собственный капитал", description: "Остаток по чистым активам"}],

    [EconomyElements.treasuryNegEquity, {name: "Neg.Equity", translation: "Отриц. собственный капитал", description: "Чистые активы (отрицательные)"}],
    [EconomyElements.treasuryTDeposits, {name: "T.Deposits", translation: "Счета правительтва", description: "Депозиты правительства (депозиты правительства в Центральном банке, используемые для осуществления госрасходов. Эти депозиты являются активом для правительства и обязательством для Центрального банка)"}],
    [EconomyElements.treasuryTTL, {name: "TT&L", translation: "Депозиты правительства в коммерческих банках", description: "TT&L в практике ФРС США — операция, заключающаяся в переводе налоговых платежей обратно на счета коммерческих банков (вместо счетов правительства в ЦБ) для того, чтобы не создавать резкие колебания в объёме доступных резервов"}],
    [EconomyElements.treasuryTreasuries, {name: "Treasuries", translation: "ОФЗ", description: "Ценные бумаги казначейства (облигации и другие финансовые активы, в которых осуществляет, помимо прочего, сбережения частный сектор)"}],

    [EconomyElements.centralBankCurrency, {name: "Currency", translation: "Наличные", description: "Ценные бумаги (преимущественно облигации казначейства, но также и некоторые ценные бумаги частного сектора. В случае открытой экономики среди активов Центрального банка присутствуют золотовалютные резервы. Совокупные активы ЦБ уравновешены его обязательствами — главным образом, резервами коммерческих банков, наличными, депозитами правительства)"}],
    [EconomyElements.centralBankDeposits, {name: "Deposits", translation: "Депозиты", description: "Депозиты состоят из депозитов правительства, с одной стороны, и коммерческих банков (во втором случае речь идёт о резервах)"}],
    [EconomyElements.centralBankEquity, {name: "Equity", translation: "Собственный капитал", description: "Остаток по чистым активам"}],
    [EconomyElements.centralBankTreasuries, {name: "Treasuries", translation: "ОФЗ", description: "Ценные бумаги (преимущественно облигации казначейства, но также и некоторые ценные бумаги частного сектора. В случае открытой экономики среди активов Центрального банка присутствуют золотовалютные резервы. Совокупные активы ЦБ уравновешены его обязательствами — главным образом, резервами коммерческих банков, наличными, депозитами правительства)"}],

    [EconomyElements.banksCurrency, {name: "Currency", translation: "Наличные", description: "Кэш (наличные деньги, накопленное богатство данного сектора)"}],
    [EconomyElements.banksDeposits, {name: "Deposits", translation: "Депозиты", description: "Депозиты (в основном вклады домохозяйств и фирм разной срочности и с разными ставками)"}],
    [EconomyElements.banksEquity, {name: "Equity", translation: "Собственный капитал", description: "Собственный капитал банков (из этих средств возмещаются потери от невозвращённых кредитов, если эта величина сократится ниже определённого минимума или станет отрицательной, банк может быть признан банкротом)"}],
    [EconomyElements.banksLoans, {name: "Loans", translation: "Кредиты", description: "Предоставление банками финансирования фирмам и домохозяйствам в долг под процент. В результате операции заёмщик приобретает средства на банковском депозите, которые, являясь обязательством коммерческого банка, при этом обладают ликвидностью, сопоставимой с фиатными деньгами (наличными), и тоже могут использоваться как средство платежа."}],
    [EconomyElements.banksReserves, {name: "Reserves", translation: "Резервы", description: "Резервы (счета коммерческих банков в Центральном банке, которые можно обменять на наличные деньги по требованию)"}],
    [EconomyElements.banksTTL, {name: "TT&L", translation: "Депозиты правительства в коммерческих банках", description: "TT&L в практике ФРС США — операция, заключающаяся в переводе налоговых платежей обратно на счета коммерческих банков (вместо счетов правительства в ЦБ) для того, чтобы не создавать резкие колебания в объёме доступных резервов"}],
    [EconomyElements.banksTreasuries, {name: "Treasuries", translation: "ОФЗ", description: "Ценные бумаги (преимущественно облигации казначейства, но также и некоторые ценные бумаги частного сектора. В случае открытой экономики среди активов Центрального банка присутствуют золотовалютные резервы. Совокупные активы ЦБ уравновешены его обязательствами — главным образом, резервами коммерческих банков, наличными, депозитами правительства)"}],


    [EconomyElements.householdBonds, {name: "Bonds", translation: "Облигации", description: "Облигации — В данном случае долговые обязательства правительства различной срочности."}],
    [EconomyElements.householdCurrency, {name: "Currency", translation: "Наличные", description: "Кэш (наличные деньги, накопленное богатство данного сектора)"}],
    [EconomyElements.householdDeposits, {name: "Deposits", translation: "Депозиты", description: "Депозиты в коммерческих банках и других кредитных организациях частного сектора"}],
    [EconomyElements.householdEquity, {name: "Equity", translation: "Собственный капитал", description: "Остаток по чистым активам (если все финансовые активы домохозяйств предполагают обязательства перед другими обязательствами, на уровне сектора домохозяйств в целом финансовые активы будут равны нулю)"}],
    [EconomyElements.householdLoans, {name: "Loans", translation: "Кредиты", description: "Предоставление банками финансирования фирмам и домохозяйствам в долг под процент. В результате операции заёмщик приобретает средства на банковском депозите, которые, являясь обязательством коммерческого банка, при этом обладают ликвидностью, сопоставимой с фиатными деньгами (наличными), и тоже могут использоваться как средство платежа."}],
    [EconomyElements.householdTreasuries, {name: "Treasuries", translation: "ОФЗ", description: "Сбережения в виде облигаций и наличных денег"}],

    [EconomyElements.companiesBonds, {name: "Bonds", translation: "Облигации", description: "Облигации — В данном случае долговые обязательства правительства различной срочности."}],
    [EconomyElements.companiesDeposits, {name: "Deposits", translation: "Депозиты", description: "Депозиты (как и домохозяйства, фирмы держат свои активы в качестве банковских депозитов для проведения сделок)"}],
    [EconomyElements.companiesEquity, {name: "Equity", translation: "Собственный капитал", description: "Остаток по чистым активам"}],
    [EconomyElements.companiesSecurLoans, {name: "Sec.Loans", translation: "Секьюритизированные кредиты", description: "Понятие секьюритизированных кредитов связано с выпуском ценных бумаг, обеспеченных кредитами (обычно ипотечными). В результате секьюритизации кредиты списываются с баланса банка и уступаются финансовому посреднику, а финансовый посредник создаёт облигации, обеспеченные указанными кредитами. С помощью секьюритизации банк перекладывает риск невозврата кредита другой организации, взамен получая денежные средства."}],
  ])


  ngOnInit(): void {
    this.initialize();
    this.selectedOperation  = Operations.operations[0];
  }

  public ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private showError(error: string){
    this.error  = error;
    const errorTimer = timer(5000);
    if (this.subscription)
      this.subscription.unsubscribe();
    this.subscription = errorTimer.subscribe(() => {
        this.error = undefined;
    });
  }

  private initialize(){
    this.economy = new Economy({
      treasuryTDeposits : 40,
      treasuryTTL : 0,
      centralBankTreasuries : 170,
      centralBankEquity : 10,
      banksCurrency : 40,
      banksLoans : 0,
      banksReserves : 80,
      banksTreasuries : 0,
      householdCurrency : 0,
      householdDeposits : 40,
      householdTreasuries : 40,
      householdLoans : 0,
      companiesDeposits : 40,
      companiesBonds : 0,
      companiesSecurLoans : 0
    })
    this.updateSectors(false);
  }

  private updateSectors(calculateDelta: boolean){
    this.total = this.createSectorViewModel("Total economy aggregate", "Итог по всей экономике", this.economy.total, this.total, calculateDelta);
    this.federalGovernment = this.createSectorViewModel("Federal government", "Федеральное правительство", this.economy.federalGovernment, this.federalGovernment, calculateDelta);
    this.privateSector = this.createSectorViewModel("Private sector", "Частный сектор", this.economy.privateSector, this.privateSector, calculateDelta);
    this.treasury = this.createSectorViewModel("Treasury (Federal government)", "Минфин (или Казначейсто)", this.economy.treasury, this.treasury, calculateDelta);
    this.centralBank = this.createSectorViewModel("Central bank", "Центральный Банк", this.economy.centralBank, this.centralBank, calculateDelta);
    this.banks = this.createSectorViewModel("Banks", "Частные банки", this.economy.banks, this.banks, calculateDelta);
    this.houseHolds = this.createSectorViewModel("Households", "Домохозяйства", this.economy.households, this.houseHolds, calculateDelta);
    this.companies = this.createSectorViewModel("Companies", "Частные компании\\фирмы", this.economy.companies, this.companies, calculateDelta);
  }

  private createSectorViewModel(name: string, translation: string, sector: Sector, oldSector : ISectorViewModel, calculateDelta: boolean): ISectorViewModel{
    const assetsPrev = calculateDelta ? new Map(oldSector.assets.map(i => [i.id, i.value])) : new Map();
    const liabilitiesPrev = calculateDelta ? new Map(oldSector.liabilities.map(i => [i.id, i.value])): new Map();
    return {
      name: name,
      translation: translation,
      sum: sector.sum,
      assets: sector.assets.map((o, idx) => {
        return {
            id: o.id,
            value: o.value,
            name: this.elementDescriptions.get(o.id).name,
            translation: this.elementDescriptions.get(o.id).translation,
            color: this.colors[idx],
            delta: o.value - assetsPrev.get(o.id)
          }
      }),
      liabilities: sector.liabilities.map((o, idx) => {
        return {
            id: o.id,
            value: o.value,
            name: this.elementDescriptions.get(o.id).name,
            translation: this.elementDescriptions.get(o.id).translation,
            color: this.colors[this.colors.length - idx -1],
            delta: o.value - liabilitiesPrev.get(o.id)
        }
      }),
    }
  }


  runOperation(){
    this.error = undefined;
    try{
      this.economy.applyCommands(this.selectedOperation.func(this.value));
      this.updateSectors(true);
    }
    catch{
      this.showError("Невозможно применить данную операцию при текущих значениях");
    }
  }

  onElementClicked(elementId: string){
    this.selectedElementId = elementId;
  }

  revertLast(){
    this.economy.revertLast();
    this.updateSectors(false);
  }

  replayLast(){
    this.economy.replayLast();
    this.updateSectors(true);
  }

  reset(){
    this.initialize();
  }
}
