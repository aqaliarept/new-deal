import { EconomyElements } from "./economyElements";
import { IOperation } from "./operation";

export class Formula{
    
    adds: string[] = [];
    subs: string[] = [];

    constructor(public id: string) {
    }

    public add(id: string){
        this.adds.push(id);
        return this;
    }

    public sub(id: string){
        this.subs.push(id);
        return this;
    }

    public isDependentFrom(id: string){
        return this.adds.indexOf(id) != -1 
            || this.subs.indexOf(id) != -1;
    }

    public calculate(values : Map<string, number>) : number{
        let value = 0;
        for (let add of this.adds){
            value += values.get(add);
        }
        for (let sub of this.subs){
            value -= values.get(sub);
        }
        return value;
    }
}

class Operation implements IOperation{
    public readonly treasuryTDeposits : number = 0;
    public readonly treasuryTTL : number = 0;
    public readonly centralBankTreasuries : number = 0;
    public readonly centralBankEquity : number = 0;
    public readonly householdDeposits : number = 0;
    public readonly householdTreasuries : number = 0;
    public readonly householdCurrency : number = 0;
    public readonly householdLoans : number = 0;
    public readonly banksCurrency : number = 0;
    public readonly banksReserves : number = 0;
    public readonly banksLoans : number = 0;
    public readonly banksTreasuries : number = 0;
    public readonly companiesDeposits : number = 0;
    public readonly companiesSecurLoans : number = 0;
    public readonly companiesBonds : number = 0;

    constructor(params: Partial<IOperation>) {
        Object.entries(params).forEach(e => {
            this[e[0]] = e[1];
        });
    }

    public toArray() : {id : string, value: number}[]{
        return [
            {id : "treasuryTDeposits", value: this.treasuryTDeposits},
            {id : "treasuryTTL", value: this.treasuryTTL},
            {id : "centralBankTreasuries", value: this.centralBankTreasuries},
            {id : "centralBankEquity", value: this.centralBankEquity},
            {id : "householdDeposits", value: this.householdDeposits},
            {id : "householdTreasuries", value: this.householdTreasuries},
            {id : "householdCurrency", value: this.householdCurrency},
            {id : "householdLoans", value: this.householdLoans},
            {id : "banksCurrency", value: this.banksCurrency},
            {id : "banksReserves", value: this.banksReserves},
            {id : "banksLoans", value: this.banksLoans},
            {id : "banksTreasuries", value: this.banksTreasuries},
            {id : "companiesDeposits", value: this.companiesDeposits},
            {id : "companiesSecurLoans", value: this.companiesSecurLoans},
            {id : "companiesBonds", value: this.companiesBonds}
        ]
    }
}


class Transaction {
    changes: Map<string, number> = new Map<string, number>();
    intendation: number = 0;

    constructor(iniatialState : Map<string, number>){
        for (let key of iniatialState.keys()){ 
            this.changes.set(key, iniatialState.get(key));
        }
    }

    private log(message: string){
        console.log(`${"".padEnd(this.intendation, " ")} ${message}`);
    }

    private updateElement(id: string, value: number){
        const oldValue = this.changes.get(id);
        if (oldValue === value)
            return;
        this.log(`Element ${id} changed from [${oldValue}] to [${value}]`)
        this.changes.set(id, value);
        this.onElementChanged(id, value);
    }

    applyCommands(command : Operation) : void{
        for (let e of command.toArray().filter(o => o.value != 0)){
            this.updateElement(e.id, this.changes.get(e.id) + e.value);
        };
    }
    
    onElementChanged(id: string, value: number) {
        this.intendation += 2;
        for (let formula of Economy.formulas.filter(o => o.isDependentFrom(id))){
            let newValue = formula.calculate(this.changes);
            this.updateElement(formula.id, newValue);
        }
        this.intendation -= 2;
    }
}

export class Sector{
    public readonly isInvalid: boolean = false;
    public readonly sum?: number;
    constructor(
        public readonly assets: {id: string, value: number,}[],
        public readonly liabilities: {id: string,value: number}[]){
            if (assets.find(e => e.value < 0) || liabilities.find(e => e.value < 0)){
                throw new Error("Negative element value. " + assets.concat(liabilities).map(e => `${e.id} = ${e.value}`).join(", "));
            }
            const assetSum = assets.map(o => o.value).reduce((p, o) => p += o);
            const liabilitiesSum = assets.map(o => o.value).reduce((p, o) => p += o);
            // add sum equality validation
            this.sum = assetSum;
        }
}

export class Economy{
    public static readonly formulas : Formula[] = [
       // 1
       new Formula(EconomyElements.treasuryNegEquity)
            .add(EconomyElements.treasuryTreasuries)
            .sub(EconomyElements.treasuryTDeposits)
            .sub(EconomyElements.treasuryTTL),
        // 2
        new Formula(EconomyElements.treasuryTreasuries)
            .add(EconomyElements.householdTreasuries)
            .add(EconomyElements.banksTreasuries)
            .add(EconomyElements.centralBankTreasuries),
        // 3
        new Formula(EconomyElements.centralBankCurrency)
            .add(EconomyElements.banksCurrency)
            .add(EconomyElements.householdCurrency),       
        // 4
        new Formula(EconomyElements.centralBankDeposits)
            .add(EconomyElements.treasuryTDeposits)
            .add(EconomyElements.banksReserves),             
        // 5
        new Formula(EconomyElements.banksDeposits)
            .add(EconomyElements.householdDeposits)
            .add(EconomyElements.companiesDeposits),
        // 6
        new Formula(EconomyElements.banksEquity)
            .add(EconomyElements.banksReserves)
            .add(EconomyElements.banksLoans)
            .add(EconomyElements.banksCurrency)
            .sub(EconomyElements.banksDeposits)
            .add(EconomyElements.banksTreasuries)
            .sub(EconomyElements.banksTTL),
        // 7
        new Formula(EconomyElements.householdBonds)
            .add(EconomyElements.companiesBonds),        
        // 9
        new Formula(EconomyElements.householdEquity)
            .add(EconomyElements.householdTreasuries)
            .add(EconomyElements.householdDeposits)
            .add(EconomyElements.householdCurrency)
            .add(EconomyElements.householdBonds)
            .sub(EconomyElements.householdLoans),       
        // 10
        new Formula(EconomyElements.companiesEquity)
            .add(EconomyElements.companiesDeposits)
            .sub(EconomyElements.companiesBonds)
            .add(EconomyElements.companiesSecurLoans),
        // 11
        new Formula(EconomyElements.banksTTL)
            .add(EconomyElements.treasuryTTL),
        // FG 1
        new Formula(EconomyElements.fedGovNegEquity)
            .add(EconomyElements.treasuryNegEquity)
            .sub(EconomyElements.centralBankEquity),
        // FG 2
        new Formula(EconomyElements.fedGovAssets)
            .add(EconomyElements.treasuryTDeposits)
            .add(EconomyElements.centralBankTreasuries)
            .add(EconomyElements.treasuryTTL),
        // FG 3
        new Formula(EconomyElements.fedGovLiabilities)
            .add(EconomyElements.treasuryTreasuries)
            .add(EconomyElements.centralBankDeposits)
            .add(EconomyElements.centralBankCurrency),

        // PS 1
        new Formula(EconomyElements.privateAssets)
            .add(EconomyElements.banksCurrency)
            .add(EconomyElements.banksReserves)
            .add(EconomyElements.banksLoans)
            .add(EconomyElements.banksTreasuries)
            .add(EconomyElements.householdTreasuries)
            .add(EconomyElements.householdDeposits)
            .add(EconomyElements.householdBonds)
            .add(EconomyElements.householdCurrency)
            .add(EconomyElements.companiesDeposits)
            .add(EconomyElements.companiesSecurLoans),
        // PS 2
        new Formula(EconomyElements.privateEquity)
            .add(EconomyElements.banksEquity)
            .add(EconomyElements.householdEquity)
            .add(EconomyElements.companiesEquity),            
        // PS 3
        new Formula(EconomyElements.privateLiabilities)
            .add(EconomyElements.banksDeposits)
            .add(EconomyElements.banksTTL)
            .add(EconomyElements.householdLoans)
            .add(EconomyElements.companiesBonds),   
        // T 1
        new Formula(EconomyElements.totalAssets)
            .add(EconomyElements.fedGovAssets)
            .add(EconomyElements.privateAssets),   
        // T 2
        new Formula(EconomyElements.totalLiabilities)
            .add(EconomyElements.fedGovLiabilities)
            .add(EconomyElements.privateLiabilities),   

        ]

    private static readonly calculatedElements: Set<string> = new Set<string>(Economy.formulas.map(o => o.id));
    private values : Map<string, number> = new Map<string, number>();
    private transactions: Transaction[] = [];
    private lastRevertedTransaction: Transaction = null;

    public total : Sector;
    public privateSector: Sector;
    public federalGovernment: Sector;
    public treasury: Sector;
    public centralBank: Sector;
    public banks: Sector;
    public households: Sector;
    public companies: Sector;

    constructor(params: Partial<IOperation>){
        for (let id of EconomyElements.allElemets){
            this.values.set(id, 0);
        }
        this.applyCommands(params);
    }

    public get(id: string): any {
      return this.values.get(id);
    }

    private apply(transaction: Transaction){
        //console.log("values: " + Array.from(this.values).map(e => `${e[0]} = ${e[1]}`).join(", "));
        const values = transaction.changes;
        //console.log("changes: " + Array.from(values).map(e => `${e[0]} = ${e[1]}`).join(", "));
        const total = Economy.createSector(values, [EconomyElements.totalAssets], [EconomyElements.totalLiabilities], "Invalid total economy sector");
        const privateSector = Economy.createSector(values, [EconomyElements.privateAssets], [EconomyElements.privateLiabilities, EconomyElements.privateEquity], "Invalid private sector");
        const federalGovernment = Economy.createSector(values, [EconomyElements.fedGovNegEquity, EconomyElements.fedGovAssets], [EconomyElements.fedGovLiabilities], "Invalid federal government sector");
        const treasury = Economy.createSector(values, [EconomyElements.treasuryNegEquity, EconomyElements.treasuryTDeposits, EconomyElements.treasuryTTL], [EconomyElements.treasuryTreasuries], "Invalid treasury sector");
        const centralBank = Economy.createSector(values, [EconomyElements.centralBankTreasuries], [EconomyElements.centralBankEquity, EconomyElements.centralBankCurrency, EconomyElements.centralBankDeposits], "Invalid central bank sector");
        const banks = Economy.createSector(values, [EconomyElements.banksCurrency, EconomyElements.banksReserves, EconomyElements.banksLoans, EconomyElements.banksTreasuries], [EconomyElements.banksEquity, EconomyElements.banksDeposits, EconomyElements.banksTTL], "Invalid banks sector");
        const households = Economy.createSector(values, [EconomyElements.householdDeposits, EconomyElements.householdTreasuries, EconomyElements.householdCurrency, EconomyElements.householdBonds], [EconomyElements.householdEquity, EconomyElements.householdLoans], "Invalid household sector");
        const companies = Economy.createSector(values, [EconomyElements.companiesDeposits, EconomyElements.companiesSecurLoans], [EconomyElements.companiesEquity, EconomyElements.companiesBonds], "Invalid companies sector");
           
        this.total = total;
        this.privateSector = privateSector;
        this.federalGovernment = federalGovernment;
        this.treasury = treasury;
        this.centralBank = centralBank;
        this.banks = banks;
        this.households = households;
        this.companies = companies;
        this.values = values;
        this.transactions.push(transaction);
    }

    public applyCommands(params: Partial<IOperation>){
        const command = new Operation(params);
        let transaction = new Transaction(this.values);
        transaction.applyCommands(command);
        this.apply(transaction);
    }

    private static createSector(elements: Map<string, number> , assets: string[], liabilities: string[], message: string): Sector{
        const sector = new Sector(
            assets.map(e => {
                return {
                    id: e, 
                    value: elements.get(e)};
                }),
            liabilities.map(e => {
                return {
                    id: e, 
                    value: elements.get(e)};
                }));
        if (sector.isInvalid) {
            throw new Error(message);      
        }
        return sector;
    }

    replayLast() {
        if (!this.canReplayLast()){
          throw new Error("There is no operation to replay");
        }
        this.apply(this.lastRevertedTransaction);
        this.lastRevertedTransaction = null;
      }
      
  
      canReplayLast(): boolean {
        return this.lastRevertedTransaction !== null;
      }
  
      revertLast() {
        if (!this.canRevertLast()){
          throw new Error("There is no operation to rollback");
        }
        this.lastRevertedTransaction = this.transactions.pop();
        this.apply(this.transactions.pop());
      }
  
      canRevertLast(): boolean {
        return this.transactions.length > 1;
      }

}