import { Economy } from "./economy";
import { EconomyElements } from "./economyElements";

describe('Economy', () => {

  let economy: Economy;

  beforeEach((() => {
    economy = new Economy({
      treasuryTDeposits : 40,
      treasuryTTL : 0,
      centralBankTreasuries : 170,
      centralBankEquity : 10,
      banksCurrency : 40,
      banksLoans : 0,
      banksReserves : 80,
      banksTreasuries : 0,
      householdCurrency : 0,
      householdDeposits : 40,
      householdTreasuries : 40,
      householdLoans : 0,
      companiesDeposits : 40,
      companiesBonds : 0,
      companiesSecurLoans : 0
    })
  }));

  it('should initialize economy', () => {
    economy.applyCommands({householdDeposits : 10})
    expect(economy.get(EconomyElements.householdDeposits)).toEqual(50);
  });

  it('should change base element', () => {
    economy.applyCommands({householdCurrency : 10});
    economy.applyCommands({householdCurrency : -5});

    expect(economy.get(EconomyElements.householdCurrency)).toEqual(5);
  });

  it('should change calculated element', () => {
    expect(economy.get(EconomyElements.centralBankCurrency)).toEqual(40);

    economy.applyCommands({householdCurrency : 10});

    expect(economy.get(EconomyElements.centralBankCurrency)).toEqual(50);
  });

  it('should rollback last operation', () => {

    expect(economy.get(EconomyElements.centralBankCurrency)).toEqual(40);
    expect(economy.canRevertLast()).toEqual(false);
    expect(economy.canReplayLast()).toEqual(false);
    
    economy.applyCommands({householdCurrency : 10});
    expect(economy.get(EconomyElements.centralBankCurrency)).toEqual(50);
    expect(economy.canRevertLast()).toEqual(true);
    expect(economy.canReplayLast()).toEqual(false);
    
    economy.revertLast();
    expect(economy.get(EconomyElements.centralBankCurrency)).toEqual(40);
    expect(economy.canRevertLast()).toEqual(false);
    expect(economy.canReplayLast()).toEqual(true);

    economy.replayLast();
    expect(economy.get(EconomyElements.centralBankCurrency)).toEqual(50);
    expect(economy.canRevertLast()).toEqual(true);
    expect(economy.canReplayLast()).toEqual(false);
    
  });

  it('should throw error when negative element has been calculated', () => {
    expect(() => economy.applyCommands({treasuryTDeposits : -300})).toThrowError();
  });

  it('check default values', () => {
    let expectedValues = EconomyValuesTester.GetDefaultValues();

    EconomyValuesTester.CheckValues(economy, expectedValues);
  });
});

export class EconomyValuesTester {

  public static CheckValues(economy : Economy, expectedValues : Map<string, number>) {
    Array.from(expectedValues).forEach(element => 
      expect(economy.get(element[0])).toBe(element[1], `${element[0]} is wrong`));
  }

  public static GetDefaultValues() {
    return new Map<string, number>([
      [ EconomyElements.treasuryNegEquity,     170 ],
      [ EconomyElements.treasuryTDeposits,     40 ],
      [ EconomyElements.treasuryTTL,           0 ],
      [ EconomyElements.treasuryTreasuries,    210 ],
      [ EconomyElements.centralBankTreasuries, 170 ],
      [ EconomyElements.centralBankEquity,     10 ],
      [ EconomyElements.centralBankCurrency,   40 ],
      [ EconomyElements.centralBankDeposits,   120 ],
      [ EconomyElements.householdDeposits,     40 ],
      [ EconomyElements.householdTreasuries,   40 ],
      [ EconomyElements.householdCurrency,     0 ],
      [ EconomyElements.householdBonds,        0 ],
      [ EconomyElements.householdEquity,       80 ],
      [ EconomyElements.householdLoans,        0 ],
      [ EconomyElements.banksCurrency,         40 ],
      [ EconomyElements.banksReserves,         80 ],
      [ EconomyElements.banksLoans,            0 ],
      [ EconomyElements.banksTreasuries,       0 ],
      [ EconomyElements.banksEquity,           40 ],
      [ EconomyElements.banksDeposits,         80 ],
      [ EconomyElements.banksTTL,              0 ],
      [ EconomyElements.companiesDeposits,     40 ],
      [ EconomyElements.companiesSecurLoans,   0 ],
      [ EconomyElements.companiesEquity,       40 ],
      [ EconomyElements.companiesBonds,        0 ],
      [ EconomyElements.fedGovNegEquity,       160 ],
      [ EconomyElements.fedGovAssets,          210 ],
      [ EconomyElements.fedGovLiabilities,     370 ],
      [ EconomyElements.privateAssets,         240 ],
      [ EconomyElements.privateEquity,         160 ],
      [ EconomyElements.privateLiabilities,    80 ],
      [ EconomyElements.totalAssets,           450 ],
      [ EconomyElements.totalLiabilities,      450 ],
    ]);
  }
}

