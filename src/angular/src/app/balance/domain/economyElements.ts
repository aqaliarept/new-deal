
export class EconomyElements {
    //#region Treasury
    // assets
    public static readonly treasuryNegEquity: string = "treasuryNegEquity";
    public static readonly treasuryTDeposits: string = "treasuryTDeposits";
    public static readonly treasuryTTL: string = "treasuryTTL";
    // liabilities
    public static readonly treasuryTreasuries: string = "treasuryTreasuries";
    //#endregion
    //#region Central Bank
    // assets
    public static readonly centralBankTreasuries: string = "centralBankTreasuries";
    // liabilities
    public static readonly centralBankEquity: string = "centralBankEquity";
    public static readonly centralBankCurrency: string = "centralBankCurrency";
    public static readonly centralBankDeposits: string = "centralBankDeposits";
    //#endregion
    //#region Households
    // assets
    public static readonly householdDeposits: string = "householdDeposits";
    public static readonly householdTreasuries: string = "householdTreasuries";
    public static readonly householdCurrency: string = "householdCurrency";
    public static readonly householdBonds: string = "householdBonds";
    // liabilities
    public static readonly householdEquity: string = "householdEquity";
    public static readonly householdLoans: string = "householdLoans";
    //#endregion
    //#region Banks
    // assets
    public static readonly banksCurrency: string = "banksCurrency";
    public static readonly banksReserves: string = "banksReserves";
    public static readonly banksLoans: string = "banksLoans";
    public static readonly banksTreasuries: string = "banksTreasuries";

    // liabilities
    public static readonly banksEquity: string = "banksEquity";
    public static readonly banksDeposits: string = "banksDeposits";
    public static readonly banksTTL: string = "banksTTL";
    //#endregion
    //#region Companies
    // assets
    public static readonly companiesDeposits: string = "companiesDeposits";
    public static readonly companiesSecurLoans: string = "companiesSecurLoans";

    //liabilities
    public static readonly companiesEquity: string = "companiesEquity";
    public static readonly companiesBonds: string = "companiesBonds";
    //#endregion
    //#region Federal Covernment
    // assets
    public static readonly fedGovNegEquity: string = "fedGovNegEquity";
    public static readonly fedGovAssets: string = "fedGovAssets";

    //liabilities
    public static readonly fedGovLiabilities: string = "fedGovLiabilities";
    //#endregion
    //#region Federal Covernment
    // assets
    public static readonly privateAssets: string = "privateSectorAssets";

    //liabilities
    public static readonly privateEquity: string = "privateSectorEquity";
    public static readonly privateLiabilities: string = "privateSectorLiabilities";
    //#endregion
    //#region Total Economy
    // assets
    public static readonly totalAssets: string = "totalAssets";

    //liabilities
    public static readonly totalLiabilities: string = "totalLiabilities";
    //#endregion    
    public static readonly allElemets = [
        EconomyElements.treasuryNegEquity,
        EconomyElements.treasuryTDeposits,
        EconomyElements.treasuryTTL,
        EconomyElements.treasuryTreasuries,
        EconomyElements.centralBankTreasuries,
        EconomyElements.centralBankEquity,
        EconomyElements.centralBankCurrency,
        EconomyElements.centralBankDeposits,
        EconomyElements.householdDeposits,
        EconomyElements.householdTreasuries,
        EconomyElements.householdCurrency,
        EconomyElements.householdBonds,
        EconomyElements.householdEquity,
        EconomyElements.householdLoans,
        EconomyElements.banksCurrency,
        EconomyElements.banksReserves,
        EconomyElements.banksLoans,
        EconomyElements.banksTreasuries,
        EconomyElements.banksEquity,
        EconomyElements.banksDeposits,
        EconomyElements.banksTTL,
        EconomyElements.companiesDeposits,
        EconomyElements.companiesSecurLoans,
        EconomyElements.companiesEquity,
        EconomyElements.companiesBonds,
        EconomyElements.fedGovNegEquity,
        EconomyElements.fedGovAssets,
        EconomyElements.fedGovLiabilities,
        EconomyElements.privateAssets,
        EconomyElements.privateEquity,
        EconomyElements.privateLiabilities,
        EconomyElements.totalAssets,
        EconomyElements.totalLiabilities
    ];

    public static DoesElementExist(id: string) {
        if (!this.allElemets.find(o => o == id))
            throw new Error(`There is no element [${id}]`);
    }
}
