
export interface IOperation {
    readonly treasuryTDeposits: number;
    readonly treasuryTTL: number;
    readonly centralBankTreasuries: number;
    readonly centralBankEquity: number;
    readonly householdDeposits: number;
    readonly householdTreasuries: number;
    readonly householdCurrency: number;
    readonly householdLoans : number,
    readonly banksCurrency: number;
    readonly banksReserves: number;
    readonly banksLoans: number;
    readonly banksTreasuries: number;
    readonly companiesDeposits: number;
    readonly companiesSecurLoans: number;
    readonly companiesBonds: number;
}



